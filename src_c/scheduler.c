#include "scheduler.h"


/***************************************************
/ Print functions
****************************************************/
void print_task(struct task_s task){
    printf("Period: %d, deadline: %d, wcet: %d, fname: %s\n",
            task.period, task.deadline, task.wcet, task.fname);
    return;
}

void print_tasks(struct task_s *tasks, int nb_task){
    int i;
    for(i=0; i<nb_task; i++){
        print_task(tasks[i]);
    }
    return;
}

void usage(void){
    printf("Usage: ./scheduler <filein> <filout> <RM|DM>\n");
}

void print_scheduling(struct time_unit_s *time_units, int n){
    int i, j;
    printf("Possible scheduling \n");
    for(i=0; i<n; i++){
        printf("t=%d, tasks:", time_units[i].start_time);
        for(j=0; j< time_units[i].nb_tasks; j++){
            printf(" %s", time_units[i].tasks[j].fname);
        }
        printf("\n");
    }
    return;
}

void print_time_units(struct time_unit_s *time_units, int n){
    int i, j;
    for(i=0; i<n; i++){
        for(j=0; j<time_units[i].nb_tasks; j++){
            printf("%s %d, ", time_units[i].tasks[j].fname, time_units[i].exec_time[j]);
        }
    printf("\n");
    }
    return;
}

void write_time_units(FILE *fd, struct time_unit_s *time_units, int n){
    int i, j;
    for(i=0; i<n; i++){
        for(j=0; j<time_units[i].nb_tasks; j++){
            fprintf(fd, "%s %d, ", time_units[i].tasks[j].fname, time_units[i].exec_time[j]);
        }
        fprintf(fd, "\n");
    }
    return;
}


/***************************************************
/ Clean functions
****************************************************/
void free_time_units(struct time_unit_s *time_units, int n){
    int i;
    for(i=0; i<n; i++){
        free(time_units[i].tasks);
        free(time_units[i].exec_time);
    }
    return;
}


/* Free parameter if not null*/
void clean(int *periods, struct task_s *tasks,
           struct time_unit_s *time_units, int nb_tus)
{
    if(periods != NULL){
        free(periods);
    }
    if(tasks != NULL){
        free(tasks);
    }
    if(time_units != NULL){
        free_time_units(time_units, nb_tus);
        free(time_units);
    }
    return;
}



/***************************************************
/ Tools functions
****************************************************/
int gcd(int a, int b) {
  if (b == 0) return a;
  return gcd(b, a%b);
}

int extract_macro_period(int *periods, int nb_tasks){
    int lcm, i;
    lcm = periods[0];
    for(i=1; i<nb_tasks; i++){
        lcm = lcm * periods[i]/gcd(lcm, periods[i]);
    }
    return lcm;
}

int extract_uperiod(int *periods, int nb_tasks){
    int min, i;
    min = periods[0];
    for(i=1; i<nb_tasks; i++){
        if (periods[i] < min){
            min = periods[i];
        }
    }
    return min;
}

int comp_by_period(const void* a, const void* b){
    return (((struct task_s *)a)->period) - (((struct task_s *)b)->period);
}

int comp_by_deadline(const void* a, const void* b){
    return (((struct task_s *)a)->deadline) - (((struct task_s *)b)->deadline);
}


void sort_tasks(struct task_s *tasks, const int nb_tasks, const char * policy){
    if (strcmp(policy, "RM")){
        qsort(tasks, nb_tasks, sizeof(struct task_s), comp_by_period);
    } else {
        qsort(tasks, nb_tasks, sizeof(struct task_s), comp_by_deadline);
    }
}

/******************************************************
/ Parse task file, modif struct and return tasks number
******************************************************/
int parser(FILE *fp, struct task_s *tasks, int *periods){
    char *line, *token;
    int value[3], task_idx, idx;
    line = malloc(MAX_LINE_SIZE * sizeof(char));
    if (line == NULL){
        printf("ERROR: Cannot allocate memory for line in parser\n");
        return -1;
    }
    task_idx = 0;
    while(fgets(line, MAX_LINE_SIZE, fp) != NULL){
        idx = 0;
        token = strtok(line, ",");
        while ( idx < 3){
            value[idx] = atoi(token);
            token = strtok(NULL, ",");
            idx ++;
        }
        tasks[task_idx].period = value[0];
        tasks[task_idx].deadline = value[1];
        tasks[task_idx].wcet = value[2];
        token[strlen(token)-1] = 0;
        strcpy(tasks[task_idx].fname, token);
        periods[task_idx] = value[0];
        task_idx ++;
    }
    free(line);
    return task_idx;
}


/******************************************************
/ Init time units structure
******************************************************/
int init_time_units(struct time_unit_s *time_units, int n, int nb_tasks, int uperiod){
    int i;
    for(i=0; i<n; i++){
        time_units[i].tasks = malloc(nb_tasks * sizeof(struct task_s));
        if (time_units[i].tasks == NULL){
            printf("ERROR: Cannot allocate memory for tasks in time unit\n");
            /* Free memory previously allocated */
            free_time_units(time_units, i);
            return -1;
        }
        time_units[i].exec_time = malloc(nb_tasks * sizeof(int));
        if (time_units[i].exec_time == NULL){
            printf("ERROR: Cannot allocate memory\
                    for tasks in time unit\n");
            /* Free memory previously allocated */
            free(time_units[i].tasks);
            free_time_units(time_units, i);
            return -1;
        }
        time_units[i].sum_exec_time = 0;
        time_units[i].start_time = i*uperiod;
        time_units[i].nb_tasks = 0;
    }
    return 0;
}


/******************************************************
/ Scheduling tools
******************************************************/

int add_task_on_tu(struct task_s task, struct time_unit_s *tu, int exec_time, int uperiod)
{
    int possible_exec, index;

    possible_exec = uperiod - tu->sum_exec_time;
    index = tu->nb_tasks;
    if(possible_exec >= exec_time){
        /* Add full task  */
        tu->tasks[index] = task;
        tu->sum_exec_time += exec_time;
        tu->exec_time[index] = exec_time;
        tu->nb_tasks ++;
        return 0;
    }
    if (possible_exec != 0){
        /* Prempt  task  */
        tu->tasks[index] = task;
        tu->sum_exec_time += possible_exec;
        tu->exec_time[index] = possible_exec;
        tu->nb_tasks ++;
    }
    return exec_time - possible_exec;
}


int add_task(struct task_s task, struct time_unit_s *time_units,
            int nb_time_units, int uperiod)
{
    int i, remaining_time, next_tu;
    for(i=0; i<nb_time_units; i++){
        if((time_units[i].start_time % task.period)==0){
            remaining_time = add_task_on_tu(task, &(time_units[i]), task.wcet, uperiod);
            if(remaining_time > 0){
                next_tu = i+1;
                while((remaining_time > 0) && (next_tu <nb_time_units)){
                    remaining_time = add_task_on_tu(task, &(time_units[next_tu]), remaining_time, uperiod);
                    next_tu ++;
                }
                if(remaining_time != 0){ return -1; }
            }
        }
    }
    return 1;
}


int schedule(struct task_s *tasks, struct time_unit_s *time_units,
            int nb_time_units, int nb_tasks, int uperiod, const char *policy)
{
    int i, status;
    sort_tasks(tasks, nb_tasks, policy);

    for(i=0; i<nb_tasks; i++){
        status = add_task(tasks[i], time_units, nb_time_units, uperiod);
        if(status < 0){ return -1; }
    }
    return 0;
}



/******************************************************
/ Main
******************************************************/
int main (int argc, char **argv){
    FILE *fp;
    const char *filein, *fileout, *policy;
    struct task_s *tasks;
    struct time_unit_s *time_units;
    int nb_tasks, *periods, uperiod, macro_period, nb_time_units;
    int status;

    if (argc < 4){
        usage();
        return -1;
    }

    filein = argv[1];
    fileout = argv[2];
    policy = argv[3];

    /* Open and parse input file */
    fp = fopen(filein, "r");
    if (fp == NULL){
        printf("ERROR: Cannot open file %s\n", filein);
        return -1;
    }
    /* Allocate memory for tasks and periods */
    tasks = malloc(MAX_TASKS * sizeof(struct task_s));
    if (tasks == NULL){
        printf("ERROR: Cannot allocate memory for tasks\n");
        return -1;
    }
    periods = malloc(MAX_TASKS * sizeof(int));
    if (periods == NULL){
        printf("ERROR: Cannot allocate memory for periods\n");
        clean(NULL, tasks, NULL, 0);
        return -1;
    }

    /* Parse tasks description file */
    nb_tasks = parser(fp, tasks, periods);
    /* Close fp after parsing file */
    fclose(fp);
    /* Realloc tasks and period according to real task number */
    tasks = realloc(tasks, nb_tasks * sizeof(struct task_s));
    periods = realloc(periods, nb_tasks * sizeof(int));
    /* Calculate uperiod, macro period and time unit number */
    uperiod = extract_uperiod(periods, nb_tasks);
    macro_period = extract_macro_period(periods, nb_tasks);
    nb_time_units = macro_period/uperiod;

    /* Allocate and initialize time units structures */
    time_units = malloc(nb_time_units * sizeof(struct time_unit_s));
    if (time_units == NULL){
        printf("ERROR: Cannot allocate memory for time units\n");
        clean(periods, tasks, NULL, 0);
        return -1;
    }
    status = init_time_units(time_units, nb_time_units, nb_tasks, uperiod);
    if(status < 0){
        printf("ERROR: Fail on init time units\n");
        clean(periods, tasks, time_units, nb_time_units);
        return -1;
    }

    status = schedule(tasks, time_units, nb_time_units, nb_tasks, uperiod, policy);
    if (status == 0){
        print_scheduling(time_units, nb_time_units);
        fp = fopen(fileout, "w");
        if (fp == NULL){
            printf("ERROR: Cannot open file %s\n", fileout);
            /* Print details on stdout */
            printf("\n Details \n");
            print_time_units(time_units, nb_time_units);
        } else {
            write_time_units(fp, time_units, nb_time_units);
        }
    } else {
        printf("Not schedulable \n");
    }

    clean(periods, tasks, time_units, nb_time_units);
    return 0;
}
