/* Header file for scheduler */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_FUNC_NAME_SIZE 256
#define MAX_FILENAME_SIZE 256
#define MAX_LINE_SIZE 256
#define MAX_TASKS 20



/***************************************************
/ Structures
****************************************************/
/* Structure wich define task */
struct task_s{
    int period;
    int deadline;
    int wcet;
    char fname[MAX_FUNC_NAME_SIZE];
};

/* Structure wich define one time unit */
struct time_unit_s{
    int start_time;
    struct task_s * tasks;
    int sum_exec_time;
    int nb_tasks;
    int *exec_time;
};



/***************************************************
/ Print functions
****************************************************/
void print_task(struct task_s task);
void print_tasks(struct task_s *tasks, int nb_task);
void usage(void);
void print_scheduling(struct time_unit_s *time_units,int n);
void print_time_units(struct time_unit_s *time_units, int n);
void write_time_units(FILE *fd, struct time_unit_s *time_units, int n);


/***************************************************
/ Clean functions
****************************************************/
void free_time_units_task(struct time_unit_s *time_units, int n);
void clean(int *periods, struct task_s *tasks,
           struct time_unit_s *time_units, int nb_tus);


/***************************************************
/ Tools functions
****************************************************/
int gcd(int a, int b);
int extract_macro_period(int *periods, int nb_tasks);
int extract_uperiod(int *periods, int nb_tasks);
int comp_by_period(const void* a, const void* b);
int comp_by_deadline(const void* a, const void* b);
void sort_tasks(struct task_s *tasks, const int nb_tasks, const char *policy);


/******************************************************
/ Parse task file, modif struct and return tasks number
******************************************************/
int parser(FILE *fp, struct task_s *tasks, int *periods);


/******************************************************
/ Init time units structure
******************************************************/
int init_time_units(struct time_unit_s *time_units, int n, int nb_tasks, int uperiod);


/******************************************************
/ Scheduling tools
******************************************************/
int add_task_on_tu(struct task_s task, struct time_unit_s *tu, int exec_time, int uperiod);
int add_task(struct task_s task, struct time_unit_s *time_units,
            int nb_time_units, int uperiod);
int schedule(struct task_s *tasks, struct time_unit_s *time_units,
            int nb_time_units, int nb_tasks, int uperiod, const char *policy);
