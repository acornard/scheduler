This work was realized in for the UE STRE of the Master SIAME.
The Python and C programs offer an scheduling if the tasks can be scheduled.

The input file represents the tasks to be scheduled. Each line represents a task with the following format:
`period, deadline, wcet, function name`

The output file is used to write the scheduling details. Each line corresponds to a period with the tasks to be executed during this period and the load executed.

The program also displays the list of tasks for each period.

A Rate Monotonic (RM) or Deadline Monotonic (DM) policy  must be specified.

#### Directories
##### data
Contain input files examples
##### src_c
Contain C scheduler sources files
Use make to compile files.
##### src_python
Contain Python source file
