#!/usr/bin/bash
# -*- coding: utf-8 -*-

import sys
import csv
import numpy

uperiod     = 0
macroperiod = 0
tut         = []

class task(object):
    def __init__(self, period, deadline, wcet, fname):
        self.period    = period
        self.deadline  = deadline
        self.wcet      = wcet
        self.func_name = fname

    def __str__(self):
        p = "Period: "+ str(self.period)
        d = ", Deadline: "+ str(self.deadline)
        w = ", WCET: "+ str(self.wcet)
        f = ", func_name: "+ self.func_name
        return p+d+w+f


class time_unit(object):

    def __init__(self, start_time):
        self.tasks = []
        self.sum_exec_time = 0
        self.start_time = start_time

    def __str__(self):
        tasks_s =str(self.start_time) + " tasks:"
        for t in self.tasks:
            tasks_s += " "+t[0].func_name
        return tasks_s

    def add(self, task, exec_time):
        #print("Add task " + task.func_name + " on period " + str(self.start_time))
        possible_exec = (uperiod - self.sum_exec_time)
        if(possible_exec >= exec_time):
            self.tasks.append((task, exec_time))
            self.sum_exec_time += exec_time
            return 0
        elif(possible_exec != 0):
            self.tasks.append((task, possible_exec))
            self.sum_exec_time += possible_exec
        return exec_time - possible_exec


def add_task(task):
    global tut
    i = 0
    for t in tut:
        if((t.start_time % task.period) == 0):
            remaining_time = t.add(task, task.wcet)
            if(remaining_time > 0):
                next_tu = i+1
                while((remaining_time > 0) and (next_tu<len(tut))):
                    remaining_time = tut[next_tu].add(task, remaining_time)
                    next_tu+=1
                if(remaining_time != 0):
                    return False
        #print(str(t.start_time) + str(t))
        i += 1
    return True



def is_schedulable(tasks_list, sch_type):
    if (sch_type == 'RM'):
       tasks_list = sorted(tasks_list, key=lambda task: task.period)
    elif(sch_type == 'DM'):
       tasks_list = sorted(tasks_list, key=lambda task: task.deadline)
    for t in tasks_list:
        status = add_task(t)
        if not status:
            return status
    return True


# format: period, deadline, wcet, func_name
def parser(filename):
    global uperiod, macroperiod
    tasks_list = []
    period = []
    with open(filename) as f:
        tasks = csv.reader(f, delimiter=',')
        for t in tasks:
            tasks_list.append(task(int(t[0]), int(t[1]), int(t[2]), t[3]))
            period.append(int(t[0]))
    uperiod = min(period)
    macroperiod = numpy.lcm.reduce(period)
    #print("uperiod: " + str(uperiod) + " macroperiod: " + str(macroperiod))
    return tasks_list


def generate_time_unit_table():
    out = []
    start_time = 0
    for i in range(0, macroperiod//uperiod):
        out.append(time_unit(start_time))
        start_time += uperiod
    return out


def write_table(filename):
    global tut
    with open(filename, "w") as f:
        spamwriter = csv.writer(f, delimiter=',')
        for tu in tut:
            row = []
            print(str(tu))
            for task in tu.tasks:
                row.append(task[0].func_name)
                row.append(str(task[1]))
            spamwriter.writerow(row)


def usage():
    print("Usage: python main.py <filein> <fileout> <RM|DM>")

def main(argv):
    global tut
    if(len(argv) < 3):
        usage()
        return
    filein = argv[0]
    fileout = argv[1]
    sch_type = argv[2]
    tasks_list = parser(filein)
    tut = generate_time_unit_table()
    status = is_schedulable( tasks_list, sch_type)
    if(not status):
        print("Not schedulable")
        return
    else:
        write_table(fileout)
        print("Result is write on %s" % fileout)


if __name__ == "__main__":
    main(sys.argv[1:])
